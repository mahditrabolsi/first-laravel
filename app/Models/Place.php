<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Orchid\Screen\AsSource;
use Orchid\Filters\Filterable;
use Orchid\Attachment\Attachable;

class Place extends Model
{
    use AsSource, Filterable, Attachable;


    protected $fillable = [
        'name',
        'address',
        'city',
        'latitude',
        'longitude',
    ];
}
