<?php

namespace App\View\Components;
use Illuminate\Support\Facades\DB;
use Illuminate\View\Component;

class mapBlade extends Component
{

    public $places;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->places = DB::table('places')->get();
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return <<<'blade'

        <div class="map" id="app">
        <gmap-map :center="{lat:10.8, lng:106.6}" :zoom="7" style="width: 95%; height: 600px; margin-right:2%;
    margin-left:2%; ">
            @foreach ($places as $key => $place)
           
                <gmap-marker :position="{lat:{{ $place->latitude }}, lng:{{ $place->longitude }}}"
                    :clickable="true" :title="'{{ $place->name }}'" :label="'{{ $key + 1 }}'">
                </gmap-marker>
            @endforeach

    </div>
    <script src="{{ mix('js/app.js') }}">
blade;
    }
}
