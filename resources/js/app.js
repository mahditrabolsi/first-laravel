require("./bootstrap");

window.Vue = require("vue").default;

Vue.component(
    "example-component",
    require("./components/ExampleComponent.vue").default
);

import Vue from "vue";
import * as VueGoogleMaps from "vue3-google-maps";
Vue.use(VueGoogleMaps, {
    load: {
        key: "AIzaSyBQmXBUomZeoeOY5JGb4RyapTha9tEZ8AQ",
    },
});
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBQmXBUomZeoeOY5JGb4RyapTha9tEZ8AQ" type="text/javascript"></script>
const app = new Vue({
    el: "#app",
});
