<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PlacesSeeder extends Seeder
{

    public function run()
    {
        //create  three Places entries with latitiude 10 and longitude 106
        DB::table('places')->insert([
            'name' => 'Place 1',
            'address' => 'Address 1',
            'city' => 'City 1',
            'latitude' => 10.2,
            'longitude' => 106.3,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('places')->insert([
            'name' => 'Place 2',
            'address' => 'Address 2',
            'city' => 'City 2',
            'latitude' => 10.4,
            'longitude' => 106.1,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('places')->insert([
            'name' => 'Place 3',
            'address' => 'Address 3',
            'city' => 'City 3',
            'latitude' => 10,
            'longitude' => 106.5,
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }
}
